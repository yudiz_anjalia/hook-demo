import React,{  useEffect, useRef , useState} from 'react'
import './App.css'

const App = () => {
  const[timerDays ,setTimerDays] = useState('00');
  const[timerHours ,setTimerHours] = useState('00');
  const[timerMinutes ,setTimerMinutes] = useState('00');
  const[timerSeconds ,setTimerSeconds] = useState('00');

  let interval = useRef();
  const startTimer = () => {
    const countdownDate = new Date('March 31, 2022 00:00:00').getTime();

    
    interval = setInterval(() => {
      const now = new Date().getTime();
      const distance = countdownDate - now;
       
      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)/ (1000 * 60 * 60)));
      const minutes = Math.floor((distance % (1000 * 60 * 60))  / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if(distance < 0) {
        //stop our timer
        clearInterval(interval.current);
      }
        else {
          //update timer
          setTimerDays(days);
          setTimerHours(hours);
          setTimerMinutes(minutes);
          setTimerSeconds(seconds);
        }
    }, 1000);
  } 

  //componentdidmount

  useEffect(() => {
    startTimer();
    return() => {
      clearInterval();
    };
  });

  return (
    <section className='timer-conatiner'>
      <section className='timer'>
        <div>
          <span className='calendar'></span>
          <h2>CountDown Timer</h2>
          <p>Countdown to a really special date.One you could or would imagine!</p>
        </div>
        <div>
          <section> 
            <p>{timerDays}</p>
            <p><small className='small'>Days</small></p>
          </section>
          <span>:</span>
          <section>
            <p>{timerHours}</p>
            <p><small className='small'>Hours</small></p>
          </section>
          <span>:</span>
          <section>
            <p>{timerMinutes}</p>
            <p><small className='small'>Minutes</small></p>
          </section>
          <span>:</span>
          <section>
            <p>{timerSeconds}</p>
            <p><small className='small'>Secondss</small></p>
          </section>
        </div>
      </section>
    </section>
   
  );
};

export default App